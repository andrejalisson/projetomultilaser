# [API do Sr. Jean](https://andrejalisson.com.br) Laravel Framework 7.30.4


# Descrição
Sistema para cálculos de preços de hoteis.


## Como ultilizar

Existe um servidor de teste [API do Sr. Jean](https://andrejalisson.com.br) 

Todas as informações que tem neste servidor é gerado automaticamente pelo sistema para ser realizado testes.

### Moedas

Através desse endpoint [api/Moedas](https://andrejalisson.com.br/api/Moeda).

É listado todas as moedas cadastradas no banco.
```
curl -H "Content-Type: application/json" \
  -X GET -d \
  http://localhost:8000/api/Moeda

```

É Cadastrado uma nova moeda através do código internacional padrão ISO 4217.
```
curl -H "Content-Type: application/json" \
  -X POST -d '{"codigo": "USD"}' \
  http://localhost:8000/api/Moeda

```

### Hoteis

Através desse endpoint [api/Hotel](https://andrejalisson.com.br/api/Hotel).

É listado todos os hoteis cadastradas no banco.
```
curl -H "Content-Type: application/json" \
  -X GET -d \
  http://localhost:8000/api/Hotel

```

É Cadastrado uma novo hotel.
```
curl -H "Content-Type: application/json" \
  -X POST -d '{"nome": "nome do hotel", "sgl": "valor do quarto", "dbl": "valor do quarto", "tlp": "valor do quarto", "qdpl": "valor do quarto"}' \
  http://localhost:8000/api/Moeda

```

### TipoVendedor

Através desse endpoint [api/TipoVendedor](https://andrejalisson.com.br/api/TipoVendedor).

É listado todos os tipos de vendedores cadastradas no banco.
```
curl -H "Content-Type: application/json" \
  -X GET -d \
  http://localhost:8000/api/TipoVendedor

```

É Cadastrado uma novo tipo de vendedor.
```
curl -H "Content-Type: application/json" \
  -X POST -d '{"nome": "nome do tipo de vendedor"}' \
  http://localhost:8000/api/TipoVendedor

```

### Vendedor

Através desse endpoint [api/Vendedor](https://andrejalisson.com.br/api/Vendedor).

É listado todos os Vendedores cadastradas no banco.
```
curl -H "Content-Type: application/json" \
  -X GET -d \
  http://localhost:8000/api/Vendedor

```

É Cadastrado uma novo tipo de vendedor.
```
curl -H "Content-Type: application/json" \
  -X POST -d '{"nome": "nome do vendedor", "tipoVendedor": "id do tipo de vendedor"}' \
  http://localhost:8000/api/TipoVendedor

```

### Margens

Através desse endpoint [api/Margem](https://andrejalisson.com.br/api/Margem).

É listado todas as Margens cadastradas no banco.
```
curl -H "Content-Type: application/json" \
  -X GET -d \
  http://localhost:8000/api/Margem

```

É Cadastrado uma nova Margem.
```
curl -H "Content-Type: application/json" \
  -X POST -d '{"margem": "porcentagem", "tipoVendedor": "id do tipo de vendedor", "moeda": "id da moeda"}' \
  http://localhost:8000/api/Margem

```

### Consulta

Através desse endpoint [api/Consulta](https://andrejalisson.com.br/api/Consulta).

É listado todas as Margens cadastradas no banco.
```
curl -H "Content-Type: application/json" \
  -X POST -d '{"quarto": "(sgl, dbl, tlp, qdpl)", "hotel": "id do hotel", "moeda": "id da moeda", "vendedor": "id do vendedor"}' \
  http://localhost:8000/api/Consulta

```
