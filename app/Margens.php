<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Margens extends Model{
    protected $table = 'margens';
    public $timestamps = false;
}
