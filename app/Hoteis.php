<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hoteis extends Model{
    protected $table = 'hotel';
    public $timestamps = false;
}
