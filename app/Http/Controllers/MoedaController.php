<?php

namespace App\Http\Controllers;

use App\Moedas;
use Illuminate\Http\Request;

class MoedaController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $moedas = Moedas::all();
        if($moedas->count() == 0){
            return response()->json([
                'Mensagem'   => 'Sem dados na tabela',
            ], 404);
        }
        return response()->json($moedas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $url = "https://economia.awesomeapi.com.br/all/".$request->codigo."-BRL";
        $infMoeda = json_decode(file_get_contents($url));
        switch ($request->codigo) {
            case 'USD':
                $dados = $infMoeda->USD;
                break;
            
            case 'CAD':
                $dados = $infMoeda->CAD;
                break;
            
            case 'AUD':
                $dados = $infMoeda->AUD;
                break;
            
            case 'EUR':
                $dados = $infMoeda->EUR;
                break;
            
            case 'GBP':
                $dados = $infMoeda->GBP;
                break;
            
            case 'ARS':
                $dados = $infMoeda->ARS;
                break;
            
            case 'JPY':
                $dados = $infMoeda->JPY;
                break;
            
            case 'CHF':
                $dados = $infMoeda->CHF;
                break;
            
            case 'CNY':
                $dados = $infMoeda->CNY;
                break;
            
            case 'YLS':
                $dados = $infMoeda->YLS;
                break;
            
            case 'BTC':
                $dados = $infMoeda->BTC;
                break;
            
            case 'LTC':
                $dados = $infMoeda->LTC;
                break;
            
            case 'ETH':
                $dados = $infMoeda->ETH;
                break;
            
            case 'XRP':
                $dados = $infMoeda->XRP;
                break;
            
            default:
                return response()->json([
                    'Mensagem'   => 'Moeda não encontrada',
                ], 404);
                break;
        }
        $moeda = new Moedas();
        $moeda->codigo = $dados->code;
        $moeda->nome = $dados->name;
        $moeda->lucro = $request->lucro;
        $moeda->save();
        return response()->json($moeda, 201);
    }
}
