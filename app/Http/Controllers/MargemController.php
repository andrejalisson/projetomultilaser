<?php

namespace App\Http\Controllers;

use App\Margens;
use Illuminate\Http\Request;

class MargemController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $margens = Margens::all();
        if($margens->count() == 0){
            return response()->json([
                'Mensagem'   => 'Sem dados na tabela',
            ], 404);
        }
        return response()->json($margens);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $margens = new Margens();
        $margens->magens = $request->porcentagem;
        $margens->TipoVendedor_id = $request->tipoVendedor;
        $margens->moeda_id = $request->moeda;
        $margens->save();
        return response()->json($margens, 201);
    }

}
