<?php

namespace App\Http\Controllers;

use App\Hoteis;
use App\Moedas;
use App\Vendedor;
use App\Margens;
use App\TipoVendedor;
use Illuminate\Http\Request;

class ConsultasController extends Controller{

    public function consulta(Request $request){
        $quarto         = $request->quarto;
        $hotel          = Hoteis::find($request->hotel);
        $moeda          = Moedas::find($request->moeda);
        $vendedor       = Vendedor::find($request->vendedor);
        $tipoVendedor   = TipoVendedor::find($vendedor->TipoVendedor_id);

        $margem = Margens::where('TipoVendedor_id', $vendedor->TipoVendedor_id)
                            ->where('moeda_id', $moeda->id)
                            ->first();
        if($margem == null){
            return response()->json([
                'Mensagem'   => 'Margem não cadastrada',
            ], 404);
        }
        switch ($quarto) {
            case 'sgl':
                $valor = $hotel->sgl;
                break;
            case 'dbl':
                $valor = $hotel->dbl;
                break;
            case 'tlp':
                $valor = $hotel->tlp;
                break;
            case 'qdpl':
                $valor = $hotel->qdpl;
                break;
            default:
            return response()->json([
                'Mensagem'   => 'Quarto não especificado',
            ], 404);
                break;
        }

        if($valor == null){
            return response()->json([
                'Mensagem'   => 'Hotel não dispoem de tal tipo de quarto.',
            ], 404);
        }
        $total = $valor + ($valor / 100 * $margem->magens);
        if($moeda->id == 1){
            $cambio = 0;
        }else{
            $url = "https://economia.awesomeapi.com.br/all/".$moeda->codigo."-BRL";
            $infMoeda = json_decode(file_get_contents($url));
            switch ($moeda->codigo) {
                case 'USD':
                    $dadosCambio = $infMoeda->USD;
                    break;
                
                case 'CAD':
                    $dadosCambio = $infMoeda->CAD;
                    break;
                
                case 'AUD':
                    $dadosCambio = $infMoeda->AUD;
                    break;
                
                case 'EUR':
                    $dadosCambio = $infMoeda->EUR;
                    break;
                
                case 'GBP':
                    $dadosCambio = $infMoeda->GBP;
                    break;
                
                case 'ARS':
                    $dadosCambio = $infMoeda->ARS;
                    break;
                
                case 'JPY':
                    $dadosCambio = $infMoeda->JPY;
                    break;
                
                case 'CHF':
                    $dadosCambio = $infMoeda->CHF;
                    break;
                
                case 'CNY':
                    $dadosCambio = $infMoeda->CNY;
                    break;
                
                case 'YLS':
                    $dadosCambio = $infMoeda->YLS;
                    break;
                
                case 'BTC':
                    $dadosCambio = $infMoeda->BTC;
                    break;
                
                case 'LTC':
                    $dadosCambio = $infMoeda->LTC;
                    break;
                
                case 'ETH':
                    $dadosCambio = $infMoeda->ETH;
                    break;
                
                case 'XRP':
                    $dadosCambio = $infMoeda->XRP;
                    break;
                
                default:
                    return response()->json([
                        'Mensagem'   => 'Moeda não encontrada',
                    ], 404);
                    break;
            }

            $cambio = round($total/$dadosCambio->bid, 2);
        }

        $dados = [
            "hotel" => $hotel->nome,
            "quarto" => $quarto,
            "tipoVendedor" => $tipoVendedor->nome,
            "moeda" => $moeda->codigo." - ".$moeda->nome,
            "valor" => $moeda->codigo." - ".round($cambio,2),
        ];

        return json_encode($dados);
        
    }
}
