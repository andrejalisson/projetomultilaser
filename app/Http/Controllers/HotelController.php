<?php

namespace App\Http\Controllers;

use App\Hoteis;
use Illuminate\Http\Request;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $hotel = Hoteis::all();
        if($hotel->count() == 0){
            return response()->json([
                'Mensagem'   => 'Sem dados na tabela',
            ], 404);
        }
        return response()->json($hotel);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $hotel = new Hoteis();
        $hotel->nome = $request->nome;
        $hotel->sgl = $request->sgl;
        $hotel->dbl = $request->dbl;
        $hotel->tlp = $request->tlp;
        $hotel->qdpl = $request->qdpl;
        $hotel->save();
        return response()->json($hotel, 201);
    }


}
