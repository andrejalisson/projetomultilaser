<?php

namespace App\Http\Controllers;

use App\TipoVendedor;
use Illuminate\Http\Request;

class TipoVendedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $revenda = TipoVendedor::all();
        if($revenda->count() == 0){
            return response()->json([
                'Mensagem'   => 'Sem dados na tabela',
            ], 404);
        }
        return response()->json($revenda);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $revenda = new TipoVendedor();
        $revenda->nome = $request->nome;
        $revenda->save();
        return response()->json($revenda, 201);
    }


    
}
