<?php

namespace App\Http\Controllers;

use App\Vendedor;
use Illuminate\Http\Request;

class VendedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $vendedor = Vendedor::all();
        if($vendedor->count() == 0){
            return response()->json([
                'Mensagem'   => 'Sem dados na tabela',
            ], 404);
        }
        return response()->json($vendedor);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $vendedor = new Vendedor();
        $vendedor->nome = $request->nome;
        $vendedor->TipoVendedor_id = $request->tipoVendedor;
        $vendedor->save();
        return response()->json($vendedor, 201);
    }    
}
