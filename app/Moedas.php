<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Moedas extends Model{
    protected $table = 'moeda';
    public $timestamps = false;
}
