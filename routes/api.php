<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::resource('Moeda', 'MoedaController');
Route::resource('TipoVendedor', 'TipoVendedorController');
Route::resource('Hotel', 'HotelController');
Route::resource('Margem', 'MargemController');

Route::post('Consulta', 'ConsultasController@consulta');
