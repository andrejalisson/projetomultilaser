<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMargensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('margens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('magens')->unsigned();
            $table->integer('TipoVendedor_id')->unsigned();
            $table->foreign('TipoVendedor_id')->references('id')->on('TipoVendedor');
            $table->integer('moeda_id')->unsigned();
            $table->foreign('moeda_id')->references('id')->on('moeda');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('margens');
    }
}
