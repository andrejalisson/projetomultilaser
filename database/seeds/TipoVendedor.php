<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoVendedor extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('TipoVendedor')->insert([
            'nome' => "Agências",
        ]);
        DB::table('TipoVendedor')->insert([
            'nome' => "Revenda",
        ]);
        DB::table('TipoVendedor')->insert([
            'nome' => "Venda Direta",
        ]);
    }
}
