<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Popular extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('moeda')->insert([
            'codigo' => "BRL",
            'nome' => "Real Brasileiro",
        ]);
        DB::table('moeda')->insert([
            'codigo' => "USD",
            'nome' => "Dólar",
        ]);
        DB::table('moeda')->insert([
            'codigo' => "EUR",
            'nome' => "Euro",
        ]);

        for ($i=0; $i < 50; $i++) { 
            DB::table('hotel')->insert([
                'nome'  => Str::random(20),
                'sgl'   => rand(0, 1) == 1 ? rand(100, 1000): null,
                'dbl'   => rand(0, 1) == 1 ? rand(100, 1000): null,
                'tlp'   => rand(0, 1) == 1 ? rand(100, 1000): null,
                'qdpl'  => rand(0, 1) == 1 ? rand(100, 1000): null,
            ]);
        }

        DB::table('margens')->insert([
            'magens' => 10,
            'TipoVendedor_id' => 1,
            'moeda_id' => 2,
        ]);
        DB::table('margens')->insert([
            'magens' => 15,
            'TipoVendedor_id' => 1,
            'moeda_id' => 3,
        ]);

        for ($i=0; $i < 50; $i++) { 
            DB::table('vendedor')->insert([
                'nome' => Str::random(20),
                'TipoVendedor_id' => rand(1, 3),
            ]);
        }
    }
}
