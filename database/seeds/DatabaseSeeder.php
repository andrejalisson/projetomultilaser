<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(){
        
        $this->call(TipoVendedor::class);
        $this->call(Popular::class);
    }
}
